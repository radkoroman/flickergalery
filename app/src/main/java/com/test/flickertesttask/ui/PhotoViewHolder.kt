package com.test.flickertesttask.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.test.flickertesttask.R
import com.test.flickertesttask.model.Photo


class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(photo: Photo?) {
        Glide.with(itemView)
            .load("https://farm${photo?.farm}.staticflickr.com/${photo?.server}/${photo?.id}_${photo?.secret}.jpg")
            .centerCrop()
            .thumbnail(0.05f)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.ic_placeholder)
            .apply(RequestOptions().override(550, 550))
            .into(itemView.findViewById(R.id.imageView))
    }

    companion object {
        fun create(parent: ViewGroup): PhotoViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.photo_view_item, parent, false)
            return PhotoViewHolder(view)
        }
    }
}
