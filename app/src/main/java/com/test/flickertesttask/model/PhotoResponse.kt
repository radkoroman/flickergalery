package com.test.flickertesttask.model


import com.google.gson.annotations.SerializedName

data class PhotoResponse(
    @SerializedName("photos")
    val photos: Photos?,
    @SerializedName("stat")
    val stat: String
)