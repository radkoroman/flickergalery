
package com.test.flickertesttask

import androidx.lifecycle.ViewModelProvider
import com.test.flickertesttask.api.FlickerApi
import com.test.flickertesttask.paging.PhotoRepository
import com.test.flickertesttask.paging.ViewModelFactory

object Injection {

    private fun providePhotoRepository(): PhotoRepository {
        return PhotoRepository(FlickerApi)
    }

    fun provideViewModelFactory(): ViewModelProvider.Factory {
        return ViewModelFactory(providePhotoRepository())
    }
}
