package com.test.flickertesttask.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object FlickerApi {

    private const val BASE_URL = "https://www.flickr.com/services/"

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(BASE_URL)
        .build()


    val retrofitService: FlickerEndpointInterface by lazy {
        retrofit.create(FlickerEndpointInterface::class.java)
    }
}