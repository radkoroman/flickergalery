package com.test.flickertesttask.api

import com.test.flickertesttask.model.PhotoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickerEndpointInterface {

    @GET("rest/?method=flickr.photos.search&format=json&nojsoncallback=1")
    suspend fun search(
        @Query("api_key") apiKey: String,
        @Query("text") term: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): PhotoResponse?

}