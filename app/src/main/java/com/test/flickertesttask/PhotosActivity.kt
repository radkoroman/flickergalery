package com.test.flickertesttask

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.test.flickertesttask.adapter.Adapter
import com.test.flickertesttask.adapter.PhotoLoadStateAdapter
import com.test.flickertesttask.databinding.SearchActivityBinding
import com.test.flickertesttask.paging.SearchPhotosViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

class PhotosActivity : AppCompatActivity() {

    private val mAdapter: Adapter = Adapter()
    private lateinit var viewModel: SearchPhotosViewModel
    private lateinit var binding: SearchActivityBinding

    private var searchJob: Job? = null

    private fun search(query: String) {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.search(query).collect {
                mAdapter.submitData(it)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SearchActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initList()
        initViewModel()
        initSearch()
    }

    private fun initList() {
        val footerAdapter = PhotoLoadStateAdapter { mAdapter.retry() }
        val gridLayoutManager = GridLayoutManager(this@PhotosActivity, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == mAdapter.itemCount && footerAdapter.itemCount > 0) {
                    2
                } else {
                    1
                }
            }
        }
        mAdapter.addLoadStateListener { loadState ->
            val isEmptyList = loadState.refresh is LoadState.NotLoading && mAdapter.itemCount == 0
            showEmptyList(isEmptyList)
        }
        binding.list.apply {
            this.adapter = mAdapter.withLoadStateFooter(footerAdapter)
            layoutManager = gridLayoutManager
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory())
            .get(SearchPhotosViewModel::class.java)

        lifecycleScope.launch {
            mAdapter.loadStateFlow
                .distinctUntilChangedBy { it.refresh }
                .filter { it.refresh is LoadState.NotLoading }
                .collect { binding.list.scrollToPosition(0) }
        }
    }

    private fun initSearch() {
        binding.searchView.apply {
            isActivated = true
            queryHint = "Type your keyword here"
            onActionViewExpanded()
            clearFocus()
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText!!.length > 2) {
                        search(newText)
                    }
                    return false
                }
            })
        }
    }

    private fun showEmptyList(isEmpty: Boolean) {
        if (isEmpty) {
            binding.noResultsView.visibility = VISIBLE
            binding.list.visibility = GONE
        } else {
            binding.noResultsView.visibility = GONE
            binding.list.visibility = VISIBLE
        }
    }

}