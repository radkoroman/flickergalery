package com.test.flickertesttask.paging

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.test.flickertesttask.api.FlickerApi
import com.test.flickertesttask.model.Photo
import kotlinx.coroutines.flow.Flow


class PhotoRepository(private val service: FlickerApi) {

    fun getSearchResultStream(query: String): Flow<PagingData<Photo>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { PhotosPagingSource(service, query) }
        ).flow
    }

}