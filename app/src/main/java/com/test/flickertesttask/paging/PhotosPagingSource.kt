package com.test.flickertesttask.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.test.flickertesttask.BuildConfig
import com.test.flickertesttask.api.FlickerApi
import com.test.flickertesttask.model.Photo

private const val STARTING_PAGE_INDEX = 1
internal const val NETWORK_PAGE_SIZE = 20

class PhotosPagingSource(
    private val service: FlickerApi,
    private val query: String
) : PagingSource<Int, Photo>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = service.retrofitService.search(BuildConfig.API_KEY, query, position, params.loadSize)
            val photos = response?.photos?.photo ?: emptyList()
            val nextKey = if (photos.isEmpty()) {
                null
            } else {
                position + (params.loadSize / NETWORK_PAGE_SIZE)
            }
            LoadResult.Page(
                data = photos,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: Exception) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}